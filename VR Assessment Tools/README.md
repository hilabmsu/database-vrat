# VR Assessment Tools



The folder contains the source code for  Virtual Reality Assessment Tool (VR-AT).




 We used Unity, to develop two different versions (calm and stress) of the VR-AT. 

 Both calm and stress versions had the validated SAM (Valence, Arousal, Dominance) questionnaire.

 The stress version had an additional question where participants reported their stress level using a 9-point Visual Analog Scale (VAS) (Figure~\ref{fig:vrat}: A).

 In both versions, the participant can uise the HTC Vive Pro Eye controller to report:
 - A - their SAM (Arousal, Dominance and Valence) using a slider with values ranging from 1 - 9 
 - B - radio-buttons to report the emotion felt 
 - C - stress levels during the experiment. 





![alternativetext](Images/VR-AT.png)






```python

```
