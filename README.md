# Stress Database structure

The dataset is organized so that each subject has three folders (SX, where X = subject ID).
Each subject folder contains the following folders:


### Physiological Data folder (PhysiologicalData_X): 
- CX.acq: the control data for the subject SX
- SX.acq: the stress data for the subject SX

### Subjective Measures Data (Subjective_X):
- X_VR_calm: the SAM reports for calm
- X_VR_stress: the SAM reports for stress

### Participant information folder (ParticipantInfo_X):
- X_ParticipantInfo




## Participants 

- 25 subjects participated in the study
- Mean Age: 22.28
- 15 male
- 10 female

### Data Collection

- Data collected using Biopac (ACQ format)

### Collected data have 2 channels:

- Respiration
- Electrocardiogram


## Study Protocol

The stress and the calm data presented in this investigation were derived using a sequence of two virtual reality experiences. The study was conducted in the lab at Montana State University. Prior to exposing the participants to the virtual reality experiences, they were outfitted with sensors that captured the physiological signals. For collecting the ECG data, a 3-lead chest configuration was adopted, and for the respiration data, a respiration belt that went around the chest was utilized. After placing the sensors, the real-time signals were checked, and five minutes of baseline data were collected. Following the baseline, the participants were outfitted with the HTC Vive Pro Eye HMD and were assisted in adjusting the headset so that it was comfortable. The next step was the practice phase, where participants were instructed on how to use the VR controllers to navigate in an application and select an option by pointing the controller and pressing the trigger button. Once the participants were comfortable with the controllers, they were asked to rest for three minutes to control for the effect of practice on physiological data. The following image depicts the study protocol.


![alternativetext](Images/protocol2.jpeg)


As mentioned earlier, the participants were exposed to two different VR scenarios to induce specific emotions, and the order of the VR scenarios was counterbalanced to control for order effects. Upon completing each VR session, the participants were administered SAM and emotional state questionnaire within the virtual environment to preserve their presence. Additionally, a 9-point VAS questionnaire was administered after the stress-inducing VR scenario. Further, between each VR experience, the participants were given a three-minute rest period to control the carry-forward effect of one experience on the physiological data. During the experience and the rest period, one of the researchers continuously monitored the physiological data to assure that they were collected and transmitted without any issues.
The VR experience that aimed at inducing stress consisted of four parts, and the total duration of the experience was approximately five minutes. In the first part, the participant was asked to enter an elevator and press the button to level six three times, and suddenly a big spider approaches the participant and tries to get inside the elevator.  However, the elevator door closes before the spider could enter and the elevator takes the participant to the 80th floor of the building. In the next part, on arriving at the top of the building, the participant was instructed to step off the elevator and walk on narrow plank hundreds of feet above the ground to answer a phone placed at the end of the plank. After answering the phone, the plank suddenly breaks, and the participant falls. In the third part, the participant is in a dark room, and a scary person holding a drill approaches the participant and tries to attack them with a drill. For the last part, the participant is standing in the middle of a street and is instructed to look around, where a high-speed bus suddenly approaches the participant without letting them move or react.


![alternativetext](Images/stress1.png)



The calm scenario utilized a Guided Meditation VR application. The guided meditation was based on a controlled breathing exercise in which participants were guided to slow and deepen their breath. The selected VR environment (Lost Woods) was a mountainous forest accompanied by nature sounds and lasted ten minutes.



![alternativetext](Images/calm1.png)


```python

```
